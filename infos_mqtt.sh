#!/bin/bash

id='MG_006'
veh='411'
source /etc/boemb/common.sh

while true
do
	Sortie="{\"CPU\":\"$(awk '{print $1}' /proc/loadavg)\""
	ramtot=$(grep MemTotal /proc/meminfo | awk '{print $2}')
	ramfree=$(grep MemAvailable /proc/meminfo | awk '{print $2}')
	Sortie="$Sortie,\"RAM\":\"$(((ramtot-ramfree)*100/ramtot))\""
	disks=$(df | grep '^/dev/' | awk '{print $1}')
	for d in $disks
	do
	    used=$(df -h | grep "^$d" | awk '{print $5}' | grep -oE '[0-9]*')
	    Sortie="$Sortie,\"$d\":\"$used\""
	done
	# WIFI (if on)
	if [[ $(ifplugstatus wlp1s0 | grep "link beat detected") != "" ]]
	then
	    level=$(iw dev wlp1s0 link | grep signal | grep -oE '[0-9]+')
	    Sortie="$Sortie,\"WIFI\":\"-$level\""
	fi
	# 3G (if on)
	if [[ $(ifplugstatus ppp0 | grep "link beat detected") != "" ]]
	then
	    level=$(comgt -d /dev/ttyHS4 sig | grep -oE -m1 ' [0-9]+' | cut -c2-)
	    Sortie="$Sortie,\"3G\":\"$level\""
	fi
	lastPubUpdate=$(date +"%Y/%m/%d %H:%M:%S" --date "@$(cat ${PUBLICITYLASTUPDATE})")
	Sortie="$Sortie,\"MajPub\":\"$lastPubUpdate\""
	temp=$(sensors coretemp-isa-0000 | grep 'Physical id 0' | awk '{print $4}' | grep -oE '[0-9]{2}')
	Sortie="$Sortie,\"TempCarteMere\":\"$temp\""
	sensors coretemp-isa-0000 | grep -oE 'Core [0-9]' |
	while read cpu
	do
	    temp=$(sensors coretemp-isa-0000 | grep "$cpu" | awk '{print $3}' | grep -oE '[0-9]{2}')
	    Sortie="$Sortie,\"TempCore$cpu\":\"$temp\""
	done
	Sortie="$Sortie}"
	mosquitto_pub --quiet -h 10.3.19.1 -t N/$veh/sysinfo -i $id -m "$Sortie"

	for i in `seq 1 20`
	do
                status=`curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.status' | cut -d '"' -f 2`
                if [ "$status" == "" ] 
                then 
                        continue 
                fi

		Sortie="{\"ignition\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.ignition')"
		Sortie="$Sortie,\"Ligne\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.line_number')"
		Sortie="$Sortie,\"DistFromTerminus\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.meter_offset')"
		Sortie="$Sortie,\"Arret\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.stop_requested')"
		Sortie="$Sortie,\"x\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.x')"
		Sortie="$Sortie,\"y\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.y')"
		Sortie="$Sortie,\"status\":$(curl -s -m 2 "http://127.0.0.1:8080/?SERVICE=vehicle_informations&of=json&srid=4326" | jq '.vehicle_informations.status')}"
		mosquitto_pub --quiet -h 10.3.19.1 -t N/$veh/VehInfo -i $id -m "$Sortie"
		sleep 6
	done
done
